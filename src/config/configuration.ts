export default () => ({
  contractAddress: process.env.CONTRACT_ADDRESS,
  infuraApiKey: process.env.INFURA_API_KEY,
});
