import {
  Controller,
  Get,
  Param,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { AppService } from './app.service';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('group-ids')
  @ApiResponse({
    status: 200,
    description: '​ getGroupIds ​ method in smart contract',
    schema: {
      type: 'array',
      default: ['12', '13'],
    },
  })
  findAllGroupIds() {
    return this.appService.findAllGroupIds().catch((err) => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
  }

  @Get('group/:id')
  @ApiResponse({
    status: 200,
    description: '​ getGroupIds ​ method in smart contract',
    schema: {
      type: 'object',
      default: {
        '0': 'Other Indexes',
        '1': ['7', '8', '9', '10', '11'],
        name: 'Other Indexes',
        indexes: ['7', '8', '9', '10', '11'],
      },
    },
  })
  findGroupId(@Param('id') id: string) {
    return this.appService.findGroupId(id).catch((err) => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
  }

  @Get('index/:id')
  @ApiResponse({
    status: 200,
    description: '​ getGroupIds ​ method in smart contract',
    schema: {
      type: 'object',
      default: {
        '0': 'DeFi Index (0)',
        '1': '50000000000000000',
        '2': '10000',
        '3': '240000000',
        '4': '55',
        name: 'DeFi Index (0)',
        ethPriceInWei: '50000000000000000',
        usdPriceInCents: '10000',
        usdCapitalization: '240000000',
        percentageChange: '55',
      },
    },
  })
  findIndexId(@Param('id') id: string) {
    return this.appService.findIndexId(id).catch((err) => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
  }
  
  @Get('last-block')
  @ApiResponse({
    status: 200,
    description: '​ getGroupIds ​ method in smart contract',
    schema: {
      type: 'object',
      default: {
        difficulty: '33455981',
        extraData: '0xd683010a01846765746886676f312e3136856c696e7578',
        gasLimit: 8000000,
        gasUsed: 1272245,
        hash:
          '0xe28905511586b218ab765a1c6b96d3de1c2c35323f96d0c6d1299acd7d8b5386',
        logsBloom:
          '0x00208000000000000000000080200000000000004000000008010000000001000000000000000000000000000000000000000000089000020000109000600080040000800000040000000088000000200000000000000004008002008000000000000000020002000000008000110820000000000000000000100010000000000000001000000810004000000000000000800001000000180000004000000000060000000002008000000000010000000000000000000100000000000001000000000002000004040000040000000100000000000000001000000000010022000010000400002008000000000000000000002000100000400008000000600000',
        miner: '0x1Cdb00D07b721B98Da52532DB9a7D82D2A4bF2e0',
        mixHash:
          '0x060e3896583f4117343c86801a44db459fc25001f8f823d3deffca4e9a126ee2',
        nonce: '0x5c5717ab0a093ed3',
        number: 9834165,
        parentHash:
          '0xf5a1358a8de6cf73dffe9a80920af3b15de07aced104fdd9dfb1fb3f96680d38',
        receiptsRoot:
          '0x0db0cd0fbb20859f206ab3e49648befcd1fed373839b9ff152baf0edf8d2eb31',
        sha3Uncles:
          '0x1dcc4de8dec75d7aab85b567b6ccd41ad312451b948a7413f0a142fd40d49347',
        size: 4435,
        stateRoot:
          '0x33337b49e15bd592c31b3702605b392a69e3dfc11fcc756f9ae75353c2201046',
        timestamp: 1615734082,
        totalDifficulty: '32973365329271715',
        transactions: [
          '0x63b800606707a5779290c46c34b02b1c026dd0eaade8efc4858987712ad17090',
          '0x4135a818725b56e1ddc1f7772f6bcdc0a9645ee36d23f2b5c83c8cb0f49e9c8e',
          '0xd4620286bfa0d686d9d1f0728f9b9aea58bdb30f350f1726427c85610350c8f6',
          '0xc0bbad03670b3528a8904efdae54d2ef2be1c283b38c029ee803875ba0fbb450',
          '0x4553dedd15849560b91a02f49a245ee4a8249e9de231322bee8ad685ba5bf13d',
          '0xac8b51cae748ae0ebd91093a0e239aab4f800331d0a07531244559a0347e1adb',
          '0xdd393e6cf4ae875970819cbbd9d8cac0bc11b0a141143e8b4d8105222cfbd275',
          '0xccbdad0f16c76e3edf13f6e228dfed8633ca581549b882cc1325930788fdaaa5',
        ],
        transactionsRoot:
          '0x5664e0a8c24accfa28218789ea44cc3136e0ca4d4f55b1fca1390a406dbc6cdc',
        uncles: [],
      },
    },
  })
  findLastBlock() {
    return this.appService.findLastBlock().catch((err) => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
  }
}
