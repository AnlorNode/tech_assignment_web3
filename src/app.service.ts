import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Web3 from 'web3';
import contractJson from './contract';

@Injectable()
export class AppService {
  private web3: Web3;
  private address: string;
  private contractJson: any = contractJson;
  private contract: any;
  constructor(private configService: ConfigService) {
    this.init();
  }

  init() {
    console.log(this.configService.get('contractAddress'));
    const infuraApiKey = this.configService.get('infuraApiKey');
    this.address = this.configService.get('contractAddress');
    const testNet = `https://ropsten.infura.io/v3/${infuraApiKey}`;
    this.web3 = new Web3(testNet);
    this.contract = new this.web3.eth.Contract(this.contractJson, this.address);
  }

  async findAllGroupIds() {
    return await this.contract.methods.getGroupIds().call();
  }

  async findGroupId(id: string) {
    return await this.contract.methods.getGroup(id).call();
  }

  async findIndexId(id: string) {
    return await this.contract.methods.getIndex(id).call();
  }
  
  async findLastBlock() {
    let blockNumber = await this.web3.eth.getBlockNumber();
    return await this.web3.eth.getBlock(blockNumber);
  }
}
